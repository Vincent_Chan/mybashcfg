```shell
# alias
alias l='ls -lGh'
alias ls='ls -G'
alias grepr="grep -R --color=auto -n"
alias grep='grep --color'
alias rm='rm -i'
alias jsoncurl="curl $1 | python -m json.tool"

# git
alias gitdiff="git diff --color"
alias gitloggraph="git log --oneline --graph --all --decorate --color"
alias gitloggraphhead="git log --oneline --graph --all --decorate --color | head -n20"
alias gitbranch_delete="git branch -d"
alias gitcommitamend="git commit --amend"
alias gitcheckoutoriginmaster="git remote update; git checkout origin/master"
alias gitupdateoriginmaster="git remote update"
alias gitstatusless="git status | less"
alias gitstatus="git status | head -n30"
alias gitcheckout="git checkout origin/master && git remote update && git checkout origin/master"
alias gitrebaseoriginmaster="git rebase origin/master"
alias gitpushmaster="git push origin HEAD:refs/for/master"
alias gitpushdraft="git push origin HEAD:refs/drafts/master"
```
